<?php


namespace Periode;


use Periode\Exception\PeriodException;

include_once 'AbstractPeriode.php';

abstract class AbstractPeriode
{

    private $dateDebut;
    private $dateFin;

    public function __construct($dateDebut, $dateFin)
    {
        //Je vérifie la cohérence de la date
        if ($dateDebut >= $dateFin) {
            throw new PeriodException(['debut' => $dateDebut, 'fin' => $dateFin]);
        }
        $this->setDateDebut($dateDebut);
        $this->setDateFin($dateFin);
    }

    /**
     * @return \DateTime
     */
    public function getDateDebut()
    {
        return $this->dateDebut;
    }

    /**
     * @param $dateDebut
     * @throws \Exception
     */
    public function setDateDebut($dateDebut): void
    {
        $this->dateDebut = new \DateTime($dateDebut);
    }

    /**
     * @return \DateTime
     */
    public function getDateFin()
    {
        return $this->dateFin;
    }

    /**
     * @param string $dateFin
     */
    public function setDateFin($dateFin): void
    {
        $this->dateFin = new \DateTime($dateFin);
    }
}