<?php


namespace Periode\Exception;


use Throwable;

class PeriodException extends \Exception
{
    const CODE_ERROR= 1;

    public function __construct($date = "", $code = 0, Throwable $previous = null)
    {
        parent::__construct('Date incohérente, date début : "' . $date['debut'] . '", date fin : "' . $date['fin'] .'"', self::CODE_ERROR, $previous);
    }
}