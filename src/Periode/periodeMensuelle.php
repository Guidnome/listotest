<?php

namespace Periode;

class periodeMensuelle extends AbstractPeriode
{
    public function isInclusDansPeriode(Absence $absence): bool
    {
        //Vérifie que ma période d'absence ne croise pas ma période mensuelle
        if ($absence->getDateDebut() >= $this->getDateFin() ||
        $absence->getDateFin() <= $this->getDateDebut()) {
            return false;
        }

        return true;
    }
}