<?php
namespace Periode;

include_once '../../src/Periode/Absence.php';
include_once '../../src/Periode/periodeMensuelle.php';
include_once '../../src/Periode/Exception/PeriodException.php';

use Periode\Exception\PeriodException;
use PHPUnit\Framework\TestCase;

class periodeMensuelleTest extends TestCase
{

    public function datasIsInclusDansPeriode()
    {
        return array (
            'DecembreSansConge' => array(
                array('debut' => '2020-11-15 00:00:00', 'fin' => '2020-11-30 23:59:59'),//Periode de conge
                array('debut' => '2020-12-01 00:00:00', 'fin' => '2020-12-31 23:59:59'),//Mois en cour
                false,
            ),
            'CongeSurNovembreDecembreJanvier' => array(
                array('debut' => '2020-11-15 00:00:00', 'fin' => '2021-01-15 23:59:59'),//Periode de conge
                array('debut' => '2020-12-01 00:00:00', 'fin' => '2020-12-31 23:59:59'),//Mois en cour
                true,
            ),
            'CongeSurDecembreJanvier' => array(
                array('debut' => '2020-12-15 00:00:00', 'fin' => '2021-01-15 23:59:59'),//Periode de conge
                array('debut' => '2020-12-01 00:00:00', 'fin' => '2020-12-31 23:59:59'),//Mois en cour
                true,
            ),
            'CongeSurNovembreDecembre' => array(
                array('debut' => '2020-11-15 00:00:00', 'fin' => '2020-12-15 23:59:59'),//Periode de conge
                array('debut' => '2020-12-01 00:00:00', 'fin' => '2020-12-31 23:59:59'),//Mois en cour
                true,
            ),
            'CongeSurDecembre' => array(
                array('debut' => '2020-12-15 00:00:00', 'fin' => '2020-12-31 23:59:59'),//Periode de conge
                array('debut' => '2020-12-01 00:00:00', 'fin' => '2020-12-31 23:59:59'),//Mois en cour
                true,
            ),
            'DateAbsenceIncoherente' => array(
                array('debut' => '2020-12-31 00:00:00', 'fin' => '2020-12-15 23:59:59'),//Periode de conge
                array('debut' => '2020-12-01 00:00:00', 'fin' => '2020-12-31 23:59:59'),//Mois en cour
                PeriodException::CODE_ERROR,
            ),
            'DatePeriodMensuelleIncoherente' => array(
                array('debut' => '2020-12-15 00:00:00', 'fin' => '2020-12-31 23:59:59'),//Periode de conge
                array('debut' => '2020-12-31 00:00:00', 'fin' => '2020-12-15 23:59:59'),//Mois en cour
                PeriodException::CODE_ERROR,
            ),
        );
    }

    /**
     * @dataProvider datasIsInclusDansPeriode
     */
    public function testIsInclusDansPeriode($datasConges, $dataPeriodeMensuelle, $expected)
    {
        try {
            $absence = new Absence($datasConges['debut'], $datasConges['fin']);
            $periode = new periodeMensuelle($dataPeriodeMensuelle['debut'], $dataPeriodeMensuelle['fin']);
            $result = $periode->isInclusDansPeriode($absence);

            $this->assertSame($expected, $result);
        } catch (PeriodException $exception) {
            $this->assertSame($expected, $exception->getCode());
        }
    }

}
